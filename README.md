# K8s the Hard Way

My attempt at:
https://github.com/kelseyhightower/kubernetes-the-hard-way/

- Replaced Google with AWS
- Ran from a bastion within the AWS VPC, instead of locally
- Used terraform for initial infrastructure
- Installed go and built cfssl... manually, instead of downloading
