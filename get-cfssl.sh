#!/bin/bash

set +ex

# Remove old go
if [ ! -d /usr/local/go ]; then
    # Download new go
    # This download link doesn't work direct -- have to do this part manually
    #curl -O https://golang.org/dl/go1.15.2.linux-amd64.tar.gz
    sudo tar -C /usr/local/ -xzf  go1.15.2.linux-amd64.tar.gz
    ln -s /usr/local/go/bin/go /usr/local/bin/go
fi

rpm -q gcc || sudo yum install -y gcc

go get -u github.com/cloudflare/cfssl/cmd/cfssl
go get -u github.com/cloudflare/cfssl/cmd/cfssljson
export PATH=$PATH:~/go/bin