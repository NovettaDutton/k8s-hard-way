#!/bin/bash

KUBECTL_VERSION="1.18.6"
KUBECTL_URL="https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl"
KUBECTL_PATH="/usr/local/bin/kubectl"

# Weird pipe to tee to avoid running curl as root
curl "$KUBECTL_URL" \
  | sudo tee "$KUBECTL_PATH" \
  > /dev/null

sudo chmod ugo+rx "$KUBECTL_PATH"

kubectl version --client
