#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cfssl gencert \
  -ca $SCRIPT_DIR/root-ca/ca.pem \
  -ca-key $SCRIPT_DIR/root-ca/ca-key.pem \
  -config $SCRIPT_DIR/ca-config.json \
  -profile=k8s \
  "$@"
