#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

$SCRIPT_DIR/sign-role-cert.sh user-admin system:masters
$SCRIPT_DIR/sign-role-cert.sh kube-controller-manager system:kube-controller-manager
$SCRIPT_DIR/sign-role-cert.sh kube-proxy system:node-proxier
$SCRIPT_DIR/sign-role-cert.sh kube-scheduler system:kube-scheduler