#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

DUMP_ITEMS_SCRIPT='
import json
import sys

for instance in json.load(sys.stdin):
  print("{} {} {}".format(instance["name"], instance["dns"], instance["ip"]))
'

function print_ips {
  terraform output -state=../tf/terraform.tfstate -json instances \
    | python3 -c "$DUMP_ITEMS_SCRIPT"
}

print_ips | while read name dns ip; do
    echo $name // $dns // $ip
    [ -d "$SCRIPT_DIR/$name" ] || mkdir "$SCRIPT_DIR/$name"
    echo '*' > "$SCRIPT_DIR/$name/.gitignore"
    cat > "$SCRIPT_DIR/$name/csr-config.json" <<EOF
{
  "CN": "system:node:${name}",
  "key": {
    "algo": "rsa",
    "size": 4096
  },
  "names": [
    {
      "C": "US",
      "ST": "Georgia",
      "L": "Atlanta",
      "O": "system:nodes",
      "OU": "Kubernetes The Hard Way"
    }
  ],
  "hosts": [
      "$name",
      "$dns",
      "$ip"
  ]
}
EOF
    $SCRIPT_DIR/sign-cert.sh "$SCRIPT_DIR/$name/csr-config.json" \
      | tee "$SCRIPT_DIR/$name/output.json" \
      | cfssljson -bare "$SCRIPT_DIR/$name/node"
done
