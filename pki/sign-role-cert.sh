#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
CERT_NAME="$1"
CERT_ROLE="$2"

[ -d "$SCRIPT_DIR/$CERT_NAME" ] || mkdir "$SCRIPT_DIR/$CERT_NAME"

cat > "$SCRIPT_DIR/$CERT_NAME/config.json" <<EOF
{
  "CN": "$CERT_NAME",
  "key": {
    "algo": "rsa",
    "size": 4096
  },
  "names": [
    {
      "C": "US",
      "ST": "Georgia",
      "L": "Atlanta",
      "O": "$CERT_ROLE",
      "OU": "Kubernetes The Hard Way"
    }
  ]
}
EOF

$SCRIPT_DIR/sign-cert.sh "$SCRIPT_DIR/$CERT_NAME/config.json" \
  | tee "$SCRIPT_DIR/$CERT_NAME/output.json" \
  | cfssljson -bare "$SCRIPT_DIR/$CERT_NAME/$CERT_NAME"
