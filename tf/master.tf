resource "aws_instance" "master" {
  ami           = var.ami_id
  instance_type = var.master_instance_type
  key_name      = aws_key_pair.k8s_keypair.key_name
  count         = 3
  subnet_id     = var.subnets[ count.index % length(var.subnets)]
  vpc_security_group_ids = [ aws_security_group.master_sg.id ]
  tags = {
    Name = "master-${count.index}"
  }
}

resource "aws_security_group" "master_sg" {
  name        = "master_sg"
  description = "Master SG"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    # cidr_blocks = ["0.0.0.0/0"]
    security_groups = [ var.bastion_sg_id ]
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    self = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "master_sg"
  }
}