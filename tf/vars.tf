
variable "vpc_id" { 
    default = "vpc-0e056d9fe9ac6e014"
}

variable "bastion_sg_id" {
    default = "sg-07dd992462cda2345"
}

variable "subnets" {
    type=list
    default = [
        "subnet-089c27e5bbddcd866",
        "subnet-076b46a97679a58fd",
        "subnet-04b6fc481d437bb3f"
    ]
}

variable "ami_id" {
    default = "ami-0affd4508a5d2481b"
}

variable "master_instance_type" {
    default = "t3a.large"  # 2 vCPU, 8G RAM
}

variable "worker_instance_type" {
    default = "t3a.large"  # 2 vCPU, 8G RAM
}