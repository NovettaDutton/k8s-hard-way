output "instances" {
  value = [
    for instance in concat(aws_instance.master, aws_instance.worker):
    { 
        ip = instance.private_ip
        dns = instance.private_dns
        name = instance.tags.Name
    }
  ]
}