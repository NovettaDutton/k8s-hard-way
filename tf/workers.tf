resource "aws_instance" "worker" {
  ami           = var.ami_id
  instance_type = var.master_instance_type
  key_name      = aws_key_pair.k8s_keypair.key_name
  count         = 3
  subnet_id     = var.subnets[ count.index % length(var.subnets)]
  vpc_security_group_ids = [ aws_security_group.worker_sg.id ]
  tags = {
    Name = "worker-${count.index}"
  }
}

resource "aws_security_group" "worker_sg" {
  name        = "worker_sg"
  description = "Worker SG"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    # cidr_blocks = ["0.0.0.0/0"]
    security_groups = [ var.bastion_sg_id ]
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    self = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "worker_sg"
  }
}